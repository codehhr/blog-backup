# 一 . 基于 hexo-theme-butterfly 的个人修改版

因为本人经常更换开发环境 , 所以对博客进行了备份 ( 我 比 较 懒 )

# 二 . 预览: [Saturday\_](https://codehhr.cn)

已打包为 `tar.gz` 文件,解压后把 `source` 目录下相应的文件更换为自己的 `markdown` 文件就能直接用了(你懂的)

**为了减小压缩包体积,我删掉了一些依赖 ( 也可能没删 ) , 解压后再装上:**

```bash
# 分别是 : 音乐,搜索,字数统计
cnpm install hexo-tag-aplayer hexo-generator-searchdb hexo-wordcount --save
```

**如果页面渲染不出来，尝试安装以下解决**

```bash
cnpm install  hexo-renderer-pug hexo-renderer-stylus hexo-renderer-jade hexo-generator-feed hexo-generator-sitemap hexo-browsersync hexo-generator-archive --save
```

**仓库地址**

> #### Gitee 地址 : https://gitee.com/codehhr/blog-backup
>
> #### Coding 地址 : https://codehhr.coding.net/public/codehhr/blog-backup/git/files

# 三 . 如果需要 `live2D` 需要再下载依赖 (可忽略)

**如果有则先清除旧的依赖**

```bash
npm uninstall hexo-helper-live2d
```

**安装依赖**

```bash
npm install --save hexo-helper-live2d
```

**下载 model**

我之前一直用的是 `tororo` ,还不错，挺可爱

```bash
npm install live2d-widget-model-tororo
```

> #### 模型参考 https://github.com/xiazeyu/live2d-widget-models

![tororo](https://codehhr.coding.net/p/codehhr/d/images/git/raw/master/hexo/tororo.png)

**最后在配置文件 `_config.yml` 的最下面,把 `live2d` 的 `enable` 改为 `true` 就可以了**

```yml
##Live2D看板娘
live2d:
  enable: false # true | 应用live2d
  #enable: false
  scriptFrom: local # 默认
  pluginRootPath: live2dw/ # 插件在站点上的根目录(相对路径)
  pluginJsPath: lib/ # 脚本文件相对与插件根目录路径
  pluginModelPath: assets/ # 模型文件相对与插件根目录路径
  # scriptFrom: jsdelivr # jsdelivr CDN
  # scriptFrom: unpkg # unpkg CDN
  # scriptFrom: https://cdn.jsdelivr.net/npm/live2d-widget@3.x/lib/L2Dwidget.min.js # 你的自定义 url
  tagMode: false # 标签模式, 是否仅替换 live2d tag标签而非插入到所有页面中
  debug: false # 调试, 是否在控制台输出日志
  model:
    use: live2d-widget-model-tororo
    # use: live2d-widget-model-wanko # npm-module package name
    # use: wanko # 博客根目录/live2d_models/ 下的目录名
    # use: ./wives/wanko # 相对于博客根目录的路径
    # use: https://cdn.jsdelivr.net/npm/live2d-widget-model-wanko@1.0.5/assets/wanko.model.json # 自定义 url
  display:
    position: right
    width: 150
    height: 300
  mobile:
    show: true # 是否在移动设备上显示
    scale: 0.5 # 移动设备上的缩放
  react:
    opacityDefault: 0.6
    opacityOnHover: 0.8
```

**其他的细节自己改改就行了,反正里面都有注释**
